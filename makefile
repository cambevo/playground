SHELL=/bin/bash
PREFIX=/usr
CC=$(PREFIX)/bin/gcc
OUTDIR=bin
CFLAGS=-o $(OUTDIR)/calculator

RESET=\033[0m
GREEN=\033[32m
RED=\033[31m
DONE=$(GREEN)Done$(RESET)
ERROR=$(RED)

all:
	@echo -n -e "Creating output directory... "
	@mkdir -p $(OUTDIR)
	@echo -e "$(DONE)"
	@echo -n -e "Compiling calculator.c... "
	@$(CC) $(CFLAGS) ./src/calculator.c
	@echo -e "$(DONE)"
	@echo -e "$(GREEN)Build successful$(RESET)"
